package vikram.demo.gwt4air.client;


import com.ekambi.gwt.web.core.client.flash.events.Event;
import com.ekambi.gwt.web.core.client.flash.events.FlashEventListener;
import com.ekambi.gwt.web.core.client.flash.events.MouseEvent;
import com.ekambi.gwt.web.flex.client.core.framework.FLEX;
import com.ekambi.gwt.web.flex.client.core.framework.FlexInitializationHandler;
import com.ekambi.gwt.web.flex.client.mx.containers.Form;
import com.ekambi.gwt.web.flex.client.mx.containers.FormHeading;
import com.ekambi.gwt.web.flex.client.mx.containers.FormItem;
import com.ekambi.gwt.web.flex.client.mx.containers.Panel;
import com.ekambi.gwt.web.flex.client.mx.controls.Button;
import com.ekambi.gwt.web.flex.client.mx.controls.TextArea;
import com.ekambi.gwt.web.flex.client.mx.controls.TextInput;
import com.ekambi.gwt.web.flex.client.mx.core.ContainerLayout;
import com.ekambi.gwt.web.flex.client.mx.validators.NumberValidator;
import com.ekambi.gwt.web.flex.client.mx.validators.StringValidator;
import com.ekambi.gwt.web.flex.client.spark.components.Application;
import com.google.gwt.user.client.ui.Widget;

public class FlexFormWidget {

	private Panel panel;
	private Form form;  
	private FormHeading formHeading;
	private TextInput nameInput;  
	private TextInput ageInput;  
	private TextInput countryInput;  
	private TextArea commentsInput;  
	private Button submitButton;  
	
	private Hello_gwt4air main;
	
	public FlexFormWidget(Hello_gwt4air main) {
		super();
		this.main = main;
	}

	public Widget createFlexFormWidget(){
		Widget flexWidget = FLEX.initAsWidget(new FlexInitializationHandler() {
			
			@Override
			public void onInitialization() {
				createForm();
			}
		});
		return flexWidget;
	}
	
	private void createForm(){
		
		panel = Panel.newInstance("FLEX Form");
		panel.setPercentWidth(70);  
		panel.setPercentHeight(70);  
		panel.setVerticalCenter(0);  
		panel.setHorizontalCenter(0);  
		panel.setLayout(ContainerLayout.ABSOLUTE);  
		   
		form = Form.newInstance();  
		form.setHorizontalCenter(0);  
		form.setVerticalCenter(0);  
		   
		nameInput = TextInput.newInstance();  
		ageInput = TextInput.newInstance();  
		countryInput = TextInput.newInstance();  
		commentsInput = TextArea.newInstance();  
		submitButton = Button.newInstance("Submit");
		
		nameInput.setPercentWidth(100); 
		ageInput.setPercentWidth(100);
		countryInput.setPercentWidth(100);
		commentsInput.setPercentWidth(100);
		
		formHeading = FormHeading.newInstance();
		formHeading.setLabel("Feedback Information");
		FormItem nameFormItem = FlexUtility.createFormItem("Name", true, nameInput);
		FormItem ageFormItem = FlexUtility.createFormItem("Age", true, ageInput);
		FormItem countryFormItem = FlexUtility.createFormItem("Country", true, countryInput);
		FormItem commentsFormItem = FlexUtility.createFormItem("Comments", false, commentsInput);
		
		form.addElement(formHeading);
		form.addElement(nameFormItem);  
		form.addElement(ageFormItem);
		form.addElement(countryFormItem);
		form.addElement(commentsFormItem);
		   
		FormItem buttonFormItem = FormItem.newInstance();  
		buttonFormItem.setPercentWidth(100);  

		submitButton.setPercentWidth(100);  
		buttonFormItem.addElement(submitButton);
		form.addElement(buttonFormItem);  
		panel.addElement(form);  

		submitButton.addEventListener(MouseEvent.CLICK, new FlashEventListener<Event>() {

			@Override
			protected void onFlashEvent(Event event) {
				boolean isValidated = true;
				NumberValidator ageValidator = NumberValidator.newInstance();
				ageValidator.setListener(ageInput);
				if(!FlexUtility.performValidation(ageInput.getText(), ageValidator, ageInput, "Please enter a numerical value")){
					isValidated = false;
				}
				
				StringValidator nameValidator = StringValidator.newInstance();
				nameValidator.setListener(nameInput);
				if(!FlexUtility.performValidation(nameInput.getText(), nameValidator, nameInput, "Please enter your name")){
					isValidated = false;
				}
				
				StringValidator countryValidator = StringValidator.newInstance();
				countryValidator.setListener(countryInput);
				if(!FlexUtility.performValidation(countryInput.getText(), countryValidator, countryInput, "Please enter your name")){
					isValidated = false;
				}
				
				if(isValidated){
					main.getExtGWTWidget().addInfoToGrid(nameInput.getText(), 
														 ageInput.getText(), 
														 countryInput.getText(), 
														 commentsInput.getText());
					nameInput.setText("");
					ageInput.setText("");
					countryInput.setText("");
					commentsInput.setText("");
				}
			}			
		});				 

		//Application.get().setPercentHeight(80);
		Application.get().addElement(panel); 
	}
	
	
}
