package vikram.demo.gwt4air.client;

import java.util.ArrayList;
import java.util.List;

import vikram.demo.gwt4air.shared.Contact;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.google.gwt.user.client.ui.Widget;

public class ExtGWTWidget extends LayoutContainer{

	private ColumnModel cm;
	private ListStore<Contact> store;
	
	private Hello_gwt4air main;
	
	public ExtGWTWidget(Hello_gwt4air main) {
		this.main = main;
	}

	public Widget createExtGWTGrid(){
		return createForm();
	}
	
	private LayoutContainer createForm(){  
		setLayout(new FlowLayout(0));  
		List<ColumnConfig> configs = new ArrayList<ColumnConfig>();  

		ColumnConfig column = new ColumnConfig();  
		column.setId("name");  
		column.setHeader("Name");  
		column.setWidth(150);  
		column.setRowHeader(true);  
		configs.add(column);  

		column = new ColumnConfig();  
		column.setId("age");  
		column.setHeader("Age");  
		column.setWidth(100);  
		configs.add(column);  

		column = new ColumnConfig();  
		column.setId("country");  
		column.setHeader("Country");  
		column.setWidth(75);  
		configs.add(column);  

		store = new ListStore<Contact>();  
		store.add(new Contact("Vikramaditya Bindal", 29, "India", ""));
		store.add(new Contact("Chandler Bing", 30, "New York", ""));
		store.add(new Contact("Ross Gellar", 34, "New York", ""));
		store.add(new Contact("Joey Tribiani", 33, "New York", ""));
		store.add(new Contact("Monica Gellar", 35, "New York", ""));
		cm = new ColumnModel(configs);  

		ContentPanel cp = new ContentPanel();  
		cp.setBodyBorder(true);  
		cp.setHeading("(Ext-GWT) Feedbacks from ");  
		cp.setButtonAlign(HorizontalAlignment.CENTER);  
		cp.setLayout(new FitLayout());  
		cp.setSize(600, 400);  

		final Grid<Contact> grid = new Grid<Contact>(store, cm);  
		grid.setStyleAttribute("borderTop", "none");  
		grid.setAutoExpandColumn("name");  
		grid.setBorders(true);  
		grid.setStripeRows(true);  
		grid.setLoadMask(true);
		grid.setColumnLines(true);  
		grid.setColumnReordering(false);  
		cp.add(grid);  

		add(cp); 
		return this;
	}
	
	public void addInfoToGrid(String name, String age, String country, String comments){
		int ageVal = Integer.parseInt(age);
		Contact c = new Contact(name, ageVal, country, comments);
		store.add(c);
	}
}
