package vikram.demo.gwt4air.client;


import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Padding;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

public class Hello_gwt4air implements EntryPoint {

	FlexFormWidget flexFormWidget = new FlexFormWidget(this);
	ExtGWTWidget extGWTWidget = new ExtGWTWidget(this);

	public void onModuleLoad() {
		Viewport viewPort = new Viewport();
		HBoxLayout layout = new HBoxLayout();  
		layout.setPadding(new Padding(5));  
		layout.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
		viewPort.setLayout(layout);

		viewPort.setTitle("Ext-GWT and Flex integration");

		Widget flexWidget = flexFormWidget.createFlexFormWidget();
		Widget extGwtWidget = extGWTWidget.createExtGWTGrid();

		HBoxLayoutData flex2 = new HBoxLayoutData(new Margins(5));  
		flex2.setFlex(3);
		viewPort.add(flexWidget, flex2);
		viewPort.add(extGwtWidget);

		RootPanel.get().add(viewPort);
	}

	public FlexFormWidget getFlexFormWidget() {
		return flexFormWidget;
	}

	public void setFlexFormWidget(FlexFormWidget flexFormWidget) {
		this.flexFormWidget = flexFormWidget;
	}

	public ExtGWTWidget getExtGWTWidget() {
		return extGWTWidget;
	}

	public void setExtGWTWidget(ExtGWTWidget extGWTWidget) {
		this.extGWTWidget = extGWTWidget;
	}
}