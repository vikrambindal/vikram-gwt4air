package vikram.demo.gwt4air.client;

import com.ekambi.gwt.web.flex.client.mx.containers.FormItem;
import com.ekambi.gwt.web.flex.client.mx.core.UIComponent;
import com.ekambi.gwt.web.flex.client.mx.events.ValidationResultEvent;
import com.ekambi.gwt.web.flex.client.mx.validators.Validator;

public class FlexUtility {

	public static final boolean performValidation(String text, Validator validator, UIComponent component, String errorString) {
		boolean isValidated = true;
		ValidationResultEvent validateResult = validator.validate(text);
		if(!validateResult.getType().equals(ValidationResultEvent.VALID)){
			component.setErrorString(errorString);
			isValidated = false;
		}else{
			component.setErrorString("");
		}
		return isValidated;
	}
	
	public static final FormItem createFormItem(String formLabel, boolean isRequired, UIComponent formElement){
		FormItem formItem = FormItem.newInstance();  
		formItem.setLabel(formLabel);  
		formItem.setRequired(isRequired);  
		formItem.addElement(formElement);
		return formItem;
	}
}
