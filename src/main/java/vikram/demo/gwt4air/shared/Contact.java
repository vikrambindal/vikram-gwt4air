package vikram.demo.gwt4air.shared;

import com.extjs.gxt.ui.client.data.BeanModel;

public class Contact extends BeanModel{

	private static final long serialVersionUID = -4425520750712330750L;

	private String name;
	private int age;
	private String country;
	private String comments;
	
	public Contact() {
	}

	public Contact(String name, int age, String country, String comments) {
		super();
		this.name = name;
		this.age = age;
		this.country = country;
		this.comments = comments;
		set("name", name);
		set("age", age);
		set("country", country);
		set("comments", comments);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
