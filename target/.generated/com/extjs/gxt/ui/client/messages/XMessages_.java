package com.extjs.gxt.ui.client.messages;

public class XMessages_ implements com.extjs.gxt.ui.client.messages.XMessages {
  public java.lang.String htmlEditor_justifyRightTipTitle() {
    return "Align Text Right";
  }
  
  public java.lang.String datePicker_minText() {
    return "This date is before the minimum date";
  }
  
  public java.lang.String messageBox_ok() {
    return "OK";
  }
  
  public java.lang.String dateFilter_beforeText() {
    return "Before";
  }
  
  public java.lang.String panel_expandPanel() {
    return "Expand panel";
  }
  
  public java.lang.String loadMask_msg() {
    return "Loading...";
  }
  
  public java.lang.String htmlEditor_justifyRightTipText() {
    return "Align text to the right.";
  }
  
  public java.lang.String listField_removeAll() {
    return "Remove all";
  }
  
  public java.lang.String htmlEditor_justifyLeftTipTitle() {
    return "Align Text Left";
  }
  
  public java.lang.String listField_addAll() {
    return "Add all";
  }
  
  public java.lang.String grid_ddText(int rows) {
    return "" + rows + " selected row(s)";
  }
  
  public java.lang.String htmlEditor_decreaseFontSizeTipTitle() {
    return "Shrink Text";
  }
  
  public java.lang.String htmlEditor_sourceEditTipText() {
    return "Switch to source editing mode.";
  }
  
  public java.lang.String dateFilter_afterText() {
    return "After";
  }
  
  public java.lang.String pagingToolBar_afterPageText(int pages) {
    return "of " + pages;
  }
  
  public java.lang.String colorPalette() {
    return "Color Palette";
  }
  
  public java.lang.String panel_collapsePanel() {
    return "Collapse panel";
  }
  
  public java.lang.String updateManager_indicatorText() {
    return "<div class=\"loading-indicator\">Loading...</div>";
  }
  
  public java.lang.String datePicker_prevText() {
    return "Previous Month (Control + Left)";
  }
  
  public java.lang.String htmlEditor_justifyLeftTipText() {
    return "Align  text to the left.";
  }
  
  public java.lang.String dateField_invalidText(java.lang.String date,java.lang.String format) {
    return date + " is not a valid date - it must be in the format " + format;
  }
  
  public java.lang.String htmlEditor_ulTipTitle() {
    return "Bullet List";
  }
  
  public java.lang.String datePicker_disabledDatesText() {
    return "";
  }
  
  public java.lang.String htmlEditor_italicTipTitle() {
    return "Italic";
  }
  
  public java.lang.String datePicker_todayText() {
    return "Today";
  }
  
  public java.lang.String pagingToolBar_beforePageText() {
    return "Page";
  }
  
  public java.lang.String checkBoxGroup_text(java.lang.String fieldLabel) {
    return fieldLabel + " check group";
  }
  
  public java.lang.String rowEditor_cancelText() {
    return "Cancel";
  }
  
  public java.lang.String datePicker_cancelText() {
    return "Cancel";
  }
  
  public java.lang.String datePicker_okText() {
    return "&#160;OK&#160;";
  }
  
  public java.lang.String booleanFilter_noText() {
    return "No";
  }
  
  public java.lang.String dateField_minText(java.lang.String min) {
    return "The date in this field must be after " + min;
  }
  
  public java.lang.String numberField_nanText(java.lang.String num) {
    return num + " is not a valid number";
  }
  
  public java.lang.String messageBox_yes() {
    return "Yes";
  }
  
  public java.lang.String groupingView_emptyGroupText() {
    return "(None)";
  }
  
  public java.lang.String textField_blankText() {
    return "This field is required";
  }
  
  public java.lang.String borderLayout_splitTip() {
    return "Drag to resize.";
  }
  
  public java.lang.String htmlEditor_italicTipText() {
    return "Make the selected text italic.";
  }
  
  public java.lang.String booleanFilter_yesText() {
    return "Yes";
  }
  
  public java.lang.String messageBox_close() {
    return "Close";
  }
  
  public java.lang.String pagingToolBar_nextText() {
    return "Next Page";
  }
  
  public java.lang.String htmlEditor_olTipTitle() {
    return "Numbered List";
  }
  
  public java.lang.String htmlEditor_backColorTipText() {
    return "Change the background color of the selected text.";
  }
  
  public java.lang.String datePicker_monthYearText() {
    return "Choose a month (Control+Up/Down to move years)";
  }
  
  public java.lang.String numberField_minText(double min) {
    return "The minimum value for this field is " + min;
  }
  
  public java.lang.String gridView_sortDescText() {
    return "Sort Descending";
  }
  
  public java.lang.String window_ariaMoveDescription() {
    return "Move the window using the arrow keys";
  }
  
  public java.lang.String datePicker_disabledDaysText() {
    return "";
  }
  
  public java.lang.String propertyColumnModel_valueText() {
    return "Value";
  }
  
  public java.lang.String tabPanelItem_closeText() {
    return "Close this tab";
  }
  
  public java.lang.String textField_minLengthText(int length) {
    return "The minimum length for this field is " + length;
  }
  
  public java.lang.String listField_moveSelectedDown() {
    return "Move selected down";
  }
  
  public java.lang.String htmlEditor_increaseFontSizeTipTitle() {
    return "Grow Text";
  }
  
  public java.lang.String uploadField_browseText() {
    return "Browse...";
  }
  
  public java.lang.String htmlEditor_justifyCenterTipText() {
    return "Center text in the editor.";
  }
  
  public java.lang.String dateFilter_onText() {
    return "On";
  }
  
  public java.lang.String listField_removeSelected() {
    return "Remove selected";
  }
  
  public java.lang.String themeSelector_grayTheme() {
    return "Gray Theme";
  }
  
  public java.lang.String htmlEditor_linkTipText() {
    return "Make the selected text a hyperlink.";
  }
  
  public java.lang.String htmlEditor_olTipText() {
    return "Start a numbered list.";
  }
  
  public java.lang.String listField_addSelected() {
    return "Add selected";
  }
  
  public java.lang.String messageBox_cancel() {
    return "Cancel";
  }
  
  public java.lang.String groupingView_groupByText() {
    return "Group By This Field";
  }
  
  public java.lang.String window_ariaMove() {
    return "Move";
  }
  
  public java.lang.String rowEditor_saveText() {
    return "Save";
  }
  
  public java.lang.String numericFilter_emptyText() {
    return "Enter filter text...";
  }
  
  public java.lang.String dateField_disabledDaysText() {
    return "Disabled";
  }
  
  public java.lang.String datePicker_nextText() {
    return "Next Month (Control + Right)";
  }
  
  public java.lang.String htmlEditor_foreColorTipText() {
    return "Change the Color of the selected text.";
  }
  
  public java.lang.String gridView_columnsText() {
    return "Columns";
  }
  
  public java.lang.String gridFilters_filterText() {
    return "Filters";
  }
  
  public java.lang.String textField_emptyText() {
    return "";
  }
  
  public java.lang.String pagingToolBar_displayMsg(int start,int end,int total) {
    return "Displaying " + start + " - " + end + " of " + total;
  }
  
  public java.lang.String window_ariaResizeDescription() {
    return "Resize the window using the arrow keys";
  }
  
  public java.lang.String pagingToolBar_firstText() {
    return "First Page";
  }
  
  public java.lang.String field_invalidText() {
    return "The value in this field is invalid";
  }
  
  public java.lang.String tabPanelItem_closeOtherText() {
    return "Close all other tabs";
  }
  
  public java.lang.String htmlEditor_linkTipTitle() {
    return "Hyperlink";
  }
  
  public java.lang.String stringFilter_emptyText() {
    return "Enter filter text...";
  }
  
  public java.lang.String htmlEditor_sourceEditTipTitle() {
    return "Source Edit";
  }
  
  public java.lang.String pagingToolBar_emptyMsg() {
    return "No data to display";
  }
  
  public java.lang.String pagingToolBar_lastText() {
    return "Last Page";
  }
  
  public java.lang.String datePicker_todayTip(java.lang.String date) {
    return date + " (Spacebar)";
  }
  
  public java.lang.String textField_regexText() {
    return "";
  }
  
  public java.lang.String propertyColumnModel_nameText() {
    return "Name";
  }
  
  public java.lang.String htmlEditor_boldTipTitle() {
    return "Bold";
  }
  
  public java.lang.String rowEditor_tipTitleText() {
    return "Error";
  }
  
  public java.lang.String htmlEditor_increaseFontSizeTipText() {
    return "Increase the font size.";
  }
  
  public java.lang.String htmlEditor_justifyCenterTipTitle() {
    return "Center Text";
  }
  
  public java.lang.String aria_leaveApplication() {
    return "Click tab to leave the application, or shift-tab to return to application";
  }
  
  public java.lang.String htmlEditor_boldTipText() {
    return "Make the selected text bold.";
  }
  
  public java.lang.String groupingView_showGroupsText() {
    return "Show in Groups";
  }
  
  public java.lang.String pagingToolBar_prevText() {
    return "Previous Page";
  }
  
  public java.lang.String messageBox_no() {
    return "No";
  }
  
  public java.lang.String datePicker_maxText() {
    return "This date is after the maximum date";
  }
  
  public java.lang.String htmlEditor_underlineTipTitle() {
    return "Underline";
  }
  
  public java.lang.String dateField_maxText(java.lang.String max) {
    return "The date in this field must be before " + max;
  }
  
  public java.lang.String htmlEditor_backColorTipTitle() {
    return "Text Highlight Color";
  }
  
  public java.lang.String numberField_negativeText() {
    return "The value must be greater or equal to 0s";
  }
  
  public java.lang.String htmlEditor_decreaseFontSizeTipText() {
    return "Decrease the font size.";
  }
  
  public java.lang.String pagingToolBar_refreshText() {
    return "Refresh";
  }
  
  public java.lang.String dateField_disabledDatesText() {
    return "Disabled";
  }
  
  public java.lang.String htmlEditor_ulTipText() {
    return "Start a bulleted list.";
  }
  
  public java.lang.String comboBox_loading() {
    return "Loading...";
  }
  
  public java.lang.String desktop_startButton() {
    return "Start";
  }
  
  public java.lang.String borderLayout_collapsibleSplitTip() {
    return "Drag to resize. Double click to hide.";
  }
  
  public java.lang.String textField_maxLengthText(int length) {
    return "The maximum length for this field is " + length;
  }
  
  public java.lang.String themeSelector_blueTheme() {
    return "Blue Theme";
  }
  
  public java.lang.String listField_moveSelectedUp() {
    return "Move selected up";
  }
  
  public java.lang.String window_ariaResize() {
    return "Resize";
  }
  
  public java.lang.String htmlEditor_underlineTipText() {
    return "Underline the selected text.";
  }
  
  public java.lang.String htmlEditor_createLinkText() {
    return "Please enter the URL for the link:";
  }
  
  public java.lang.String htmlEditor_foreColorTipTitle() {
    return "Font Color";
  }
  
  public java.lang.String datePicker_startDay() {
    return "0";
  }
  
  public java.lang.String gridView_sortAscText() {
    return "Sort Ascending";
  }
  
  public java.lang.String numberField_maxText(double max) {
    return "The maximum value for this field is " + max;
  }
  
  public java.lang.String radioGroup_text(java.lang.String fieldLabel) {
    return fieldLabel + " radio group";
  }
  
  public java.lang.String rowEditor_dirtyText() {
    return "You need to commit or cancel your changes";
  }
  
  }
